#=============================================
#~~~~~  Arduino Temprature Controller ~~~~~~~
#---------------------------------------------
# Interface Code
# V1.0
# Author -  Aaron Jones
# Date: 25/03/2015
# Copyright Aaron Jones, 2015
#---------------------------------------------
# This file provides a GUI for the python
# file User_Interface.py
# Please see User_Interface for further details
#
# NB This code requires PyQt4 which is released
# under the full GPLv3.
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =============================================

import sys
from PyQt4 import QtGui, QtCore
import time

# This function will spawn a new thread with Qt
class Thread(QtCore.QThread):
    def __init__(self,Function):
        QtCore.QThread.__init__(self)
	self.function = Function

    def run(self):
        self.function(True) #Specify function
        self.exec_()	#run

class MainWindow(QtGui.QWidget):
    
    def __init__(self,Ard,Temp,Ui):
        super(MainWindow, self).__init__()
        
        self.grid = QtGui.QGridLayout()
        self.grid.setSpacing(10)
        self.initUI()
	self.timer = QtCore.QTimer()
	self.timer.timeout.connect(self.events)
	self.MaxtimerVal = 100
	self.timer.start(self.MaxtimerVal)
	self.ard = Ard
	self.temp = Temp
	self.ui = Ui
       
    def initUI(self):               
        
	# Choose Default Active Sensor
	self.AS = 0	# AS = Active Sensor

	# Set up Window
	self.setGeometry(100, 100, 500, 100)
	self.windowTitle = 'Arduino Temprature Controller - Sensor '
	self.setWindowTitle(self.windowTitle + str(self.AS)) 
	
	msg = "Press Read Constants to find out :)"
	
	# Setup Buttons
	lastTempText = QtGui.QLabel('Last Recorded Temprature')
	lastTimeText = QtGui.QLabel('Time of last Recorded Temprature')
	varTempText = QtGui.QLabel('Variance')
	avTempText  = QtGui.QLabel('Average')
	peakText = QtGui.QLabel('Peak to Peak Value')
	tarTempText = QtGui.QLabel('Target Temprature')
	allowedDelta = QtGui.QLabel('Desired Stability')

	self.lastTemp = QtGui.QLabel('No Data')
	self.lastTime = QtGui.QLabel('No Data')
	self.varTemp = QtGui.QLabel('No Data')
	self.avTemp  = QtGui.QLabel('No Data')
	self.peakTemp  = QtGui.QLabel('No Data')
	self.tarTempEdit = QtGui.QLineEdit(msg)
	self.allowedDeltaEdit = QtGui.QLineEdit(msg)

	self.grid.addWidget(lastTempText, 1, 0)
	self.grid.addWidget(self.lastTemp, 1, 1)

	self.grid.addWidget(lastTimeText, 2, 0)
	self.grid.addWidget(self.lastTime, 2, 1)

	self.grid.addWidget(varTempText, 3, 0)
	self.grid.addWidget(self.varTemp, 3, 1)
	
	self.grid.addWidget(avTempText, 4, 0)
	self.grid.addWidget(self.avTemp, 4, 1)
		
	self.grid.addWidget(peakText, 5, 0)
	self.grid.addWidget(self.peakTemp, 5, 1)
		
	self.grid.addWidget(tarTempText, 6, 0)
	self.grid.addWidget(self.tarTempEdit, 6, 1)
		
	self.grid.addWidget(allowedDelta, 7, 0)
	self.grid.addWidget(self.allowedDeltaEdit, 7, 1)
		
	i = 10
		
	kpText = QtGui.QLabel('Proportional Constant')
	kiText = QtGui.QLabel('Intergral Constant')
	kdText = QtGui.QLabel('Derivitive Constant')
		
	self.kpEdit = QtGui.QLineEdit(msg)
	self.kiEdit = QtGui.QLineEdit(msg)
	self.kdEdit = QtGui.QLineEdit(msg)
		
	self.grid.addWidget(kpText, i, 0)
	self.grid.addWidget(self.kpEdit, i, 1)
		
	self.grid.addWidget(kiText, i+1, 0)
	self.grid.addWidget(self.kiEdit, i+1, 1)
		
	self.grid.addWidget(kdText, i+2, 0)
	self.grid.addWidget(self.kdEdit, i+2, 1)
		
	self.shGraph = QtGui.QPushButton('Show/Hide Graph', self)
	self.shGraph.clicked.connect(self.ShowGraph)
	self.grid.addWidget(self.shGraph,i+3,0)
		
	self.Fout = QtGui.QPushButton('Start Outputting to File', self)
	self.Fout.clicked.connect(self.fileOutput)
	self.grid.addWidget(self.Fout,i+3,1)
		
	self.sendConst = QtGui.QPushButton('Send New Constants', self)
	self.sendConst.clicked.connect(self.transConst)
	self.grid.addWidget(self.sendConst,i+4,1)
		
	self.readConst = QtGui.QPushButton('Read Constants From Device', self)
	self.readConst.clicked.connect(self.LoadVars)
	self.grid.addWidget(self.readConst,i+4,0)

	self.nextSensor = QtGui.QPushButton('Next Sensor',self)
	self.nextSensor.clicked.connect(self.chASF)
	self.grid.addWidget(self.nextSensor,i+5,0)

	self.prevSensor = QtGui.QPushButton('Prev Sensor',self)
	self.prevSensor.clicked.connect(self.chASP)
	self.grid.addWidget(self.prevSensor,i+5,1)

	self.RESET = QtGui.QPushButton('Reset Arduino',self)
	self.RESET.clicked.connect(self.arduinoReset)
	self.grid.addWidget(self.RESET,i+6,1)
		
	self.setLayout(self.grid) 
	self.show()

    def arduinoReset(self):
	self.ard.RESET()

    def ShowGraph(self):	
	title = 'Temprature of Componant ' + str(self.AS)
	ok = self.temp[self.AS].startGraph('Seconds since the epoch','Temprature (Celsius)',title)
	if ok == 1:
		self.temp[self.AS].killGraph()
        
        self.show()

    def chASF(self):	# CHange Active Sensor Forward
	self.chAS(True)

    def chASP(self):	# Change Active Sensor Previous
	self.chAS(False)

    def chAS(self,forward):	# Change Active Sensor
	if(self.temp[self.AS].graph):
		self.temp[self.AS].killGraph()
	if (forward == True):
		self.AS += 1
		if (self.AS == 6):
			self.AS = 0
	else:
		self.AS -= 1
		if (self.AS < 0):
			self.AS = 5
	
	self.lastTemp.setText('No Data')
	self.lastTime.setText('No Data')
	self.varTemp.setText('No Data')
	self.avTemp.setText('No Data')
	self.peakTemp.setText('No Data')
	self.setWindowTitle(self.windowTitle + str(self.AS))
 
        
    def fileOutput(self):
	if(self.temp[self.AS].fout):
		self.temp[self.AS].killFout()
		self.Fout.setText('Start Outputting to File')
	else:
		fName, ok1 = QtGui.QInputDialog.getText(self, 'Open File - File Name', 'Please enter a short filename:')
		desc, ok2 = QtGui.QInputDialog.getText(self, 'Open File - Description', 'Please enter a short description of the measurements:')
		if(ok1 and ok2):
			self.temp[self.AS].startFout(fName,desc)
			self.Fout.setText('Stop Outputting to File')
		
    def events(self):
	bufferGood = self.ard.bufferok()
	if (bufferGood):
		if(self.temp[self.AS].graph):
			self.temp[self.AS].reDraw()
		
		statOut = self.temp[self.AS].stats()
		if (statOut):
			self.lastTemp.setText(str(statOut[1]))
			self.lastTime.setText(time.ctime(statOut[0]))
			self.varTemp.setText(str(statOut[3]))
			self.avTemp.setText(str(statOut[2]))
			self.peakTemp.setText(str(statOut[4]))
	self.timer.start(self.MaxtimerVal)
	
    def LoadVars(self):
    	value = self.ard.getConstants(self.AS)
    	self.kpEdit.setText(value[0])
    	self.kiEdit.setText(value[1])
    	self.kdEdit.setText(value[2])
    	self.tarTempEdit.setText(value[3])
	self.allowedDeltaEdit.setText(value[4])
    	
    def transConst(self):
    	Kp = self.kpEdit.text()
    	Ki = self.kiEdit.text()
    	Kd = self.kdEdit.text()
    	setpoint = self.tarTempEdit.text()
	accuracy = self.allowedDeltaEdit.text()
	try:
		Kp = float(Kp)
		Ki = float(Ki)
		Kd = float(Kd)
		setpoint = float(setpoint)
        	accuracy = float(accuracy)
	except Exception:
	    QtGui.QMessageBox.about(self, 'Error','Input can only be a number')
	    return
	self.ard.sendConstants(Kp,Ki,Kd,setpoint,accuracy,self.AS)
	
	    
	    

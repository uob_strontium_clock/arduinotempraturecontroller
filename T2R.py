# Suitable for 3,599 < R < 32,770

from math import exp, pow

a = -1.5470381e1
b = 5.6022839e3
c = -3.7886070e5
d = 2.4971623e7
Rt = 10000

T = float(raw_input("Please enter a temprature in C between 0 and 50: "))
T = T + 273.15
const = a
linear = b/T
quad = c/(pow(T,2))
cubic = d/(pow(T,3))

R = Rt*exp(const + linear + quad + cubic)

#print const
#print linear
#print quad
#print cubic

print "The resistance is ", R, " Omhs"


#! /usr/bin/env python
#=============================================
#~~~~~  Arduino Temprature Controller ~~~~~~~
#---------------------------------------------
# Interface Code
version = 1.1
# Author -  Aaron Jones
# Date: 25/03/2015
# Copyright Aaron Jones, 2015
#---------------------------------------------
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    The inlcluded file GUI.py uses PyQt4 which is under the fuller GPL
#-------------------------------------------
# The following code is designed to be used 
# with an Arduino Nano and the custom circuit
# designed and built by Aaron Jones, 4th Yr MSci
# Physics Student, University of Birmingham.
#
# There are two modules. The first module is called
# the user module and runs on a computer 
# connected to the Arduino and allows the setting
# of variables and output of performace data.
# This is the second module called the control module
# and that runs on the Arduino actually controlling
# the temprature.
#
# Concept
# The concept is that the Arduino controls
# the temprature using a 16 Bit ADC using an I2C
# link. Proccess this infomation in this script,
# and then outputs this to a DAC back over the I2C
# link.  The voltage from the DAC is proccessed by a
# analouge circuit to control a peltier junction.
#
# A report was written on this program
# as part of a 4th Yr Project. This explains
# the method in more detail
# and contains circuit diagrams.
#
# Dependancies
# - User_Interface.py (This one)
# - Control Module (Arduino_TempController.ino)
# - Python Libaries as inported below
# - GUI.py
#
# Acknowlagements
# The code used to access the timer2 function is from: 
# http://forum.arduino.cc/index.php?topic=62964.0-
# The author is davekw7x 
#
# The code used for the 24 Bit ADC is modified from Iain MacIvers
# work in 2014 for the University of Birmingham, Cold Atoms,
# GG-TOP group studies
#
# The PID has been modified from http://playground.arduino.cc/Code/PIDLibrary
# and is written by Brett Beauregard, contact: br3ttb@gmail.com
#
# Command Characters
# The following characters are used for sending commands
# between the two programs
# - m -> Message, the next line will be a message to the user
# - h -> Handshake, please acknowledge that the script is running
# - d -> Data
#
# Constants
# In this section there are a number of user tuneable constants
# These are globally accessible and called when required
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#
# --------------------------
# User Editable Parameters
# --------------------------
#
#
# Serial Connection
# -----------------
# Please uncomment the port on which the Arduino is connected
# This can be found by opening the Arduino IDE and looking in the lower right corner
device_link = '/dev/ttyUSB0'	# Linux - Normally '/dev/ttyUSB0' or '/dev/ttyUSB1' etc
#device_link = 'COM7'		# Windows - Normally 'COM7'/'COM8' etc
#device_link = '/dev/pts/5'	# Linux - Imitation Arduino
#
# Buffer Length
#-----------------
# When data is read in from the Arudino, the last X
# values are stored in an array. This is the lenght of
# that array.
# Typical Value = 100
CONSTANT_BUFFER_LENGHT = 300
#
# Graph Length
#-----------------
# When a graph is called this is the number of points
# to plot on the X axis. This must be less than
# or equal to the buffer lenght 
# Typical Value = 75 (normally equal to buffer lenght)
CONSTANT_GRAPH_LENGHT = 300
#
# Check on buffer lenght
if (CONSTANT_BUFFER_LENGHT < CONSTANT_GRAPH_LENGHT):
	print "Please change buffer lenght to a value greater than"
	print "or equal to the graph lenght in the file User_Interface.py"
	raise SystemExit
#
# Max Number of Tempratures
# ----------------------
# The maximum number of tempratures the arduino can control
MAX_TEMPS = 6
#
#=================================================


# ===================
# PreAmble
# ==================

#Import required libaries
import serial
import thread
import struct
import sys
import time
import numpy as np
import pylab
import matplotlib.pyplot as plt


# Set up globals and locks
user_last_response = ""
user_last_response_lock = thread.allocate_lock()


# ===================
# Definitions
# ==================

# -----------------
# Main Functions
# -----------------
def prelims():
	# Welcome the user
	print('============================================')
	print('UoB Digital Temprature Controller UI Program')
	print('--------------------------------------------')
	print('Author: Aaron Jones')
	print 'Version ', version
	print "NB The GUI for this application is only available"
	print "under the full terms of the GPL."
	print('============================================')
	
	while True:
		isGUI = raw_input("\nWould you like to use the GUI for this session? (y/n):")	#Ask user whether to use the GUI
		if (isGUI == 'y'):
			isGUI = True
			return isGUI
		elif (isGUI == 'n'):
			isGUI = False
			return isGUI
		else:
			print ":( Sorry I can only accept the characters 'y' or 'n'. Lets try again?"

# Between these functions three classes, arduino, user and temprature 
# are globally defined and initilised in global scope

def main(isGUI):
	global user
	global arduino
	global temp

	if(not(isGUI)):
		# Call worker thread, program will exit when this exits
		worker(False)		
	else:
		print "Setting up GUI..."
		import GUI
		from PyQt4 import QtGui, QtCore
		print "Imports Ok!"
		app = QtGui.QApplication(sys.argv)	#Open QT Workspace
		print "Qt running. Now drawing window..."
		window = GUI.MainWindow(arduino,temp,user)
		print "Drawn! Calling worker thread..."
		# Call a worker thread to handle the Arduino and Termainal requests
		# The terminal is output only, so only certain functions will be called
		# This reduces the number of locks required
		GUI_Worker = GUI.Thread(worker)
		GUI_Worker.start()
		print "Displaying GUI in 3,2,1!"
		sys.exit(app.exec_())

# -----------------
#  Functions
# -----------------
def worker(GUI):

	# Import some global variables. 
	global user_last_response

	global user
	global arduino
	global temp

	# Connect to the Arduino
	arduino.connect()

	if (GUI):
		user_last_response_lock.acquire() # Aquire lock, blocking any input
	else:
		# spawn new thread to capture user input
		thread.start_new_thread(user.captureInput,())

	while (not(user_last_response_lock.locked())):
		time.sleep(1e-9)	#Sleep for 1 nanosecond. (Not actually possible)

	user.print2('Listening for data...')
	# Enter a Loop
	while(True):
		sys.stdout.flush()
		#Check to see if the user has requested control, is GUI open this will always be false
		if (not(user_last_response_lock.locked())):
			#Ask Arduino to go into silent mode
			user.respond(user_last_response)	# Call response function. No need to lock as currently single threaded
			thread.start_new_thread(user.captureInput,())	# ... this will relock the variable untill the user is ready
			time.sleep(1e-6)	# Small delay to allow the thread to actually start, otherwise you get wiered behaviour where the loop finished before the thread has started
		
		
		# Read the next line from the Arduino
		command, value = arduino.getLine()

		# If blank, skip
		if (command == 0):
			time.sleep(1e-6)
		# If it is a message just print it
		elif (command == arduino.msg_char):
			user.print2('Device says:')
			user.print2(value)

		# Else if it is data add it to the data 
		elif (command == arduino.data_char):
			tempratures = value.split(",")
			
			lastActiveSensor = 0	# Find the last sensor to contain data
			while (lastActiveSensor < MAX_TEMPS):
				if (tempratures[lastActiveSensor] == 'OFF'):
					break
				lastActiveSensor += 1
		
			for i in range(0,lastActiveSensor):
				temp[i].addData(time.time(),tempratures[i])
				if(not(GUI) and temp[i].graph and arduino.bufferok()):
					temp[i].reDraw() # Only redaw if the buffer is okay
					# Dont redraw if the GUI is open, let Qt handel that
		
		elif(command == arduino.handshake):
			arduino.serialInUse.acquire()
			arduino.ser.write(arduino.handshake)
			arduino.serialInUse.release()

def lotsChar(char,Num):
	string = ''
	for i in range(0,Num):
		string = string + char
	return string
# -----------------
# Classes
# -----------------

# User Input Class
class USB:
# ------------------------------
# Date: 30/12/14, Last Modified - 30/12/14
# This class holds functions for interacting with the Arduino
# ------------------------------
	def __init__(self, device_link): #Initilise class
		print 'Opening connection on', device_link
		try:
			self.ser = serial.Serial(device_link, 115200)
		except:		#print error message if this does not work
			print '\n\n\n'
			print 'No devices are available on ', device_link
			print 'Please check update where the Arduino located'
			print '(The Arduino IDE can do this, try Arduino IDE -> Tools -> Serial Port)'
			print 'and update this file'
			print '\n\n\n'
			raise

		#Set up signal characters
		self.handshake = 'h'
		self.msg_char = 'm'
		self.data_char = 'd'
		self.getVars = 'v'
		self.getConstantsRunning = False
		self.silentMode = 'S'
		
		self.serialInUse = thread.allocate_lock()
		self.serialInUse.acquire()
		self.serialInUse.release()
	
	# Arduino Connect
	def connect(self):
	# This function waits untill a Arduino with the correct software running is connected
		self.serialInUse.acquire()
		print "Attempting to find Arduino Temprature Controller"
		print "\tTry resetting the Arduino if this doesn't work"
		print "\tor 'ctrl + c' to exit the program"
		loop = True
		while(loop):
			command = self.ser.read(1)	#wait until Arduino contacts
			if (command == self.handshake):
				self.ser.write(self.handshake)	#test for handshake and respond
				while (self.ser.inWaiting() > 0):
					self.ser.read(1)
				loop = False	#exit loop
				print "Found!"
		self.serialInUse.release()

	def getLine(self):
		self.serialInUse.acquire()
		if (self.ser.inWaiting() < 2):		#Data always arrives in a minimum pair of two, so wait
			self.serialInUse.release()
			return (0,0)
		command = self.ser.read(1)
		data = self.ser.readline()
		self.serialInUse.release()
		
		return (command, data)
	
	def bufferok(self):
		if (self.ser.inWaiting() < 1):
			return True
		else:
			return False
	
	def goSilent(self,releaseLock):
		self.serialInUse.acquire()
		self.ser.write(self.silentMode)
		while(not(self.bufferok)):
			time.sleep(1e-6)
		
		while True:
			response = self.ser.read() #Clear anything on the buffer untill we reach the repsone to silent mode
			if (response == self.silentMode):
				break
		self.ser.readline(),	# Read the new line character
		
		if releaseLock:
			self.serialInUse.release()
				
	def getConstants(self,ID):
		self.goSilent(False) 		# Put arduino into silent mode, and acquire lock
			
		self.ser.write(self.getVars) #Write notification to Arduino
		self.ser.write(struct.pack('@i', ID))
		
		#Wait for line to be sent and read
		response = self.ser.readline() #self.ser.readline() # Get the response
		
		self.ser.write(self.handshake) # Send handhshake to restart sending of data
		self.serialInUse.release()	# Once we have our information unlock the buffer
		
		values = response[3:]		# Clear the charaters mV= (stands for message, containing values equals)
		Kp,Ki,Kd,Setpoint,Accuracy = values.split(',')
		
		if Accuracy[-1:] == '\n':		# Remove trailing newline char
			Accuracy = Accuracy[:-1]
			
		return (Kp),(Ki),(Kd),(Setpoint),(Accuracy)
		
	def sendConstants(self,Kp,Ki,Kd,SetPoint,Accuracy,ID):
		#print Kp, ' ',Ki, ' ',Kd, ' ',SetPoint, ' '
		self.goSilent(False)		# Put arduino into silent mode, and acquire lock
		
		self.ser.write('V')
		#self.ser.write('a')
		self.ser.write(struct.pack('>B', ID))
		self.ser.write(struct.pack('@f', Kp))
		self.ser.write(struct.pack('@f', Ki))
		self.ser.write(struct.pack('@f', Kd))
		self.ser.write(struct.pack('@f', SetPoint))
		self.ser.write(struct.pack('@f', Accuracy))
		self.ser.write('V')
		
		self.ser.write(self.handshake) # Send handhshake to restart sending of data
		self.serialInUse.release()	# Once we have our information unlock the buffer
	def RESET(self):
		self.ser.write('R')

class UI:
# ------------------------------
# Date: 30/12/14, Last Modified - 30/12/14
# This class holds functions for interacting with the user
# ------------------------------
	def __init__(self,GUIin): #Initilise class
		self.clear_msg = '                                                     '	#A set of spaces used to overwrite messages in terminal
		self.bottomLine = 'Please choose an option (press m and return for menu):'
		self.GUI = GUIin
		
	
	# User Input Function	
	def captureInput(self):
	#Function to capture key presses from the user, this should be started in a new thread
		global user_last_response	#declare variable as global to allow editing
		user_last_response_lock.acquire()	#aquire lock
		user_last_response = raw_input("")	#wait for user input and read it in
		user_last_response_lock.release()	#release lock, allowing main to see its contents
		
	def getID(self,funcName):
		ID = 'x'
		while (ID == 'x'):
			ID = raw_input(funcName + ' for which temprature?...')
			try:
				ID = int(ID)
				if (ID > 5 or ID < 0):
					raise ValueError
			except ValueError:
				print ID, ' is not a valid input, please enter an integer ID between 0 and 5 inclusive'
				ID = 'x'
		return ID
		
	def getFloat(self,message,upperlim,lowerlim):
		x = 'x'	 
		while (x == 'x'):
			x = raw_input(message)
			try:
				x = float(x)
				if (x < lowerlim or (x> upperlim)):
					raise ValueError
			except ValueError:
				print (str(x) 
					+' is not a valid input, please enter a number (e.g 1.2), between '
			 		+str(lowerlim)
			 		+' and '
			 		+str(upperlim)
			 		+' followed by the return key')
				x = 'x'
		return x
	
	def changeVal(self,name,value):
		print "The old ", name, " = ", value
		x = 'x'
		while(True):
			x = raw_input("Would you like to change this (y/n)?...")
			if (x=='y'):
				return self.getFloat("Please Enter new value for the " + name + ": ",300,-300)
			elif(x=='n'):
				return value
			else:
				print "Sorry, I can only accept the characters 'y' or 'n'"
				

	def print2(self,message):
	# This function will output data one line above some text output to the user if the GUI is not active
		if(self.GUI):
			print message
		else:
			print '\r', self.clear_msg,	#overwrite last 'Please choose...' with spaces
			print '\r', message		#print the message
			print self.bottomLine,	#re-output the bottom line
			sys.stdout.flush()	#flush so the user can actually see it
	
	def respond(self,response):	#Function to handel what happens when the user makes a request
		global arduino
		global temp
		print '\n',
		
		if(response == 'm'):		#Test for menu option
			self.display_menu()
		
		elif(response == 't'):
			ID = self.getID('Open graph')	
			title = 'Temprature of Componant ' + str(ID)
			ok = temp[ID].startGraph('Seconds since the epoch','Temprature (Celsius)',title)
			if ok == 1:
				temp[ID].killGraph()
			print 'Graph Started.',
			
		elif(response == 's'):		#Test for menu option
			ID = self.getID('Display stats')
			statOut = temp[ID].stats()
			if (not(statOut)):
				print 'Sorry no data gathered yet'
			else:
				print 'Last Recorded Time: ', time.ctime(statOut[0])
				print 'Last Recorded Temprature: ', statOut[1]
				print 'Mean Temprature: ', statOut[2]
				print 'Standard Deviation: ', statOut[3]
				print 'Peak to peak: ',statOut[4]
		elif(response == 'v'):
			ID = self.getID('Display Values')
			OutVal = arduino.getConstants(ID)
			print 'Kp = ', OutVal[0]
			print 'Ki =', OutVal[1]
			print 'Kd =', OutVal[2]
			print 'Setpoint = ', OutVal[3]
			print 'Accuracy = ', OutVal[4]
			
		elif(response == 'V'):
			ID = self.getID('Send new values')
			OutVal = arduino.getConstants(ID)
			values = list(OutVal)
			names = ["Proportional Constant", "Intergral Constant", "Derivitive Constant", "Setpoint", "Accuracy"]
			
			for i in range(0,5):
				values[i] = float(self.changeVal(names[i],values[i]))
			
			arduino.sendConstants(values[0],values[1],values[2],values[3],values[4],ID)			
			
		elif(response == 'f'):
			ID = self.getID('Output data to file')
			fileName = raw_input('Please enter a short filename: ')
			desc = raw_input('Please enter a longer description of this measurement: ')
			temp[ID].startFout(fileName,desc)
		elif(response == 'F'):
			ID = self.getID('Stop outputting data to file')
			temp[ID].killFout
		elif(response == 'e'):
			for i in range(0,MAX_TEMPS):
				if(temp[i].fout):
					temp[i].killFout
			raise SystemExit

		elif(response == 'R'):
			arduino.RESET()
		else:				#if its not valid inform the user
			print "Oops, I sorry I don't recognise that option"
		
		if (response != 'm'):
			raw_input('Please press return to continue...')	# Dont ask on menu because the menu has its own exit routine
		print '\n',
		print self.bottomLine,
		sys.stdout.flush()

	def display_menu(self):
		while(True):
			print '======== Menu ========'
			print ' - m -> display this menu'
			print ' - t -> Toggle Temprature graph'
			print ' - s -> Output Statistics'
			print ' - v -> Get Variables from Arduino'
			print ' - V -> Send new variables to Arduino'
			print ' - f -> Start outputing data to file'
			print ' - F -> Stop outputting data to file'
			print ' - R -> Reset Arduino'
			print ' - e -> Exit program'
			print '======================'
			print '\n',
			print 'NB Alway press return after an input'
			print 'the progam can only accept one character'
			print 'at a time!'
			print '\n',
			print 'Please press return to close this menu, ',
			response = raw_input("or a character to find out more...")
			if (response == ''):
				return 0
			elif (response == 'm'):
				print 'The m function displays this menu'
				print 'and lets you viewed detailed help'
				print 'on each function'
			elif (response == 't'):
				print 'Toggle the temprature graph between on and off'			
			elif (response == 's'):
				print 'Output the following statistics'
				print 'Last Recorded Time and Temprature, Mean Temprature and standard deviation'
			elif (response == 'v' or response == 'V'):
				print 'Load the proportional, intergral and differntial constanants'
				print 'and the setpoint from the Arduino and display them'
				print 'V allows changing them'
			elif (response == 'f' or response == 'F'):
				print 'Start or stop outputting data to file'
			elif (response == 'R'):
				print 'Reset the Arduino'
			elif (response == 'e'):
				print 'Exit the program fully'
			else:
				print "Sorry I don't recongnise that option"
			raw_input('\n Please press return to continue...')
# Data Class
class Data:
# ------------------------------
# Date: 23/12/14, Last Modified - 30/12/14
# This class holds data and handles the plotting of graphs
# ------------------------------
	def __init__(self,maxGraphPoints,maxStorePoints): #Initilise function
		self.xdata = []	#Create blank array for x and y data
		self.ydata = []
		self.graph = False	#No graph present yet
		self.fout = False	#Do not output data to file
		self.fHandle = False
		if (maxGraphPoints > maxStorePoints):	#The amount of data in the store must be greater than the number of store points
			raise StandardError('Number of Store points must be >= number of graph points')
		self.gPoints = maxGraphPoints	# Load maximum number of points for graph and store
		self.sPoints = maxStorePoints

	def addData(self,x,y):
		self.xdata.append(float(x))	#Append new data
		self.ydata.append(float(y))	
		#print '\n', self.ydata
		while (len(self.xdata)>self.sPoints):	#Delete old data to make room in memory for new data
			self.xdata.pop(0)
			self.ydata.pop(0)
		if(self.graph):
			self.l.set_data(self.xdata[-self.gPoints:],self.ydata[-self.gPoints:])
			# update graph data. self.l.set_data = A pointer function that allows setting of data
			# The function accepts two arguments, xdata and ydata
			# Select the data from the store, and select the last points using slicing
			# It should fail for npoints<gPoints, but python is forgiving
			self.axes.relim()		# Recalculate limits
			self.axes.autoscale_view(True,True,True) #Autoscale
		if(self.fout):
			self.fHandle.write(str(x) + ',' + str(y) + '\n')
			
	def reDraw(self):
		plt.draw()		# Redraw

	def startGraph(self,xlabel,ylabel,title):
		if (self.graph):
			return 1 #test to see if the graph is already open

		plt.ion()		# Enable interactive mode, if not already
		self.fig = plt.figure()	# Create figure, store pointer in class
		self.axes = self.fig.add_subplot(111)	# Add subplot (dont worry only one plot appears)
		self.axes.set_autoscale_on(True)	# enable autoscale
		self.axes.autoscale_view(True,True,True)
		self.l, = plt.plot([], [], 'r-')	# Plot blank data
		plt.xlabel(xlabel)			# Label Axis
		plt.ylabel(ylabel)
		plt.title(title)			# Add title
		plt.grid()				# Add grid
		self.graph = True
		return 0

	def killGraph(self):
		plt.clf()	#Clear all objects related to the figure, leave figure open
		plt.close()	# Close figure
		self.graph = False	# Inform other functions that the figure is closed
	
	def stats(self):
		if(not(self.xdata)):
			return 
		lastX = self.xdata[-1]
		lastY = self.ydata[-1]
		meanY = np.mean(self.ydata)
		stdY = np.std(self.ydata)
		peak = (np.amax(self.ydata)) - (np.amin(self.ydata))
		
		return (lastX,lastY,meanY,stdY,peak)
		
	def startFout(self,fileName,note): #Start outputing data to a file
		if (self.fout):
			global user
			user.print2('It looks like data is already being output\n')
			return
			
		startTime = time.gmtime() #Determine current time
		
		fName = 'Data/' + str(startTime[0]) + str(startTime[1]) + str(startTime[2]) + '_'
		# Generate Filename 'YYYYMMDD_HHSS-fileName
		fName = fName + str(startTime[3]) + str(startTime[4]) + '-' + fileName
		
		bufferData = open(fName+'-buff.dat','w')
		buffSize = len(self.xdata)
		for i in range(0,buffSize):
			bufferData.write(str(self.xdata[i]) + ',' + str(self.ydata[i]) + '\n')
		bufferData.close()
		
		info = open(fName+'.info','w')
		info.write(lotsChar('=',30)+'\n')
		info.write('\t UOB Temprature Controller Output File\n')
		info.write(lotsChar('=',30)+'\n\n')
		
		info.write('Date Reading Taken: ')
		info.write(time.strftime('%c',startTime)+'\n')
		info.write('Notes: ' + note + '\n')
		
		info.write(fName+'-buff.dat contains all the data on the buffer at the time that this measurement started\n')
		info.write(fName+'.dat contains data collected since the time of measurement\n')
		info.write(fName+'.info (This file) is an automatically generated infomation file\n')
		info.write('Column 1 of the data files is the date and time the column 2 data was recieved, expressed in seconds since the UNIX epoch (1st Jan 1970)\n')
		info.write('Column 2 is the data, this is temprature data expressed in degrees celsius\n')
		info.write('The data is comma seperated with a new line character expressing a new measurement\n')
		info.close()
		
		self.fHandle = open(fName+'.dat','w')
		self.fout = True
	
	def killFout(self):
		self.fHandle.close()
		self.fHandle = False
		self.fout = False

# ===================
# Call Main Function
# ==================

GLOBAL_GUI = prelims() # Call a preliminary function to start loading things

# Create Classes Globally, just about every function needs them
user = UI(GLOBAL_GUI)	# Open class to hold UI functions
arduino = USB(device_link)
#temprature = Data(CONSTANT_GRAPH_LENGHT,CONSTANT_BUFFER_LENGHT)

temp = []
for i in range(0,MAX_TEMPS):
	temp.append(Data(CONSTANT_GRAPH_LENGHT,CONSTANT_BUFFER_LENGHT))

main(GLOBAL_GUI) # Call Main to enter main loop


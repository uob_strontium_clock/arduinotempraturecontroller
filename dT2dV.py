# Suitable for 3,599 < R < 32,770

from math import exp, pow

a = -1.5470381e1
b = 5.6022839e3
c = -3.7886070e5
d = 2.4971623e7
Rt = 10000

CEL = 273.15
T1 = CEL + float(raw_input("Please enter a temprature in C between 0 and 50: "))
T2 = T1 + float(raw_input("Please enter a signal size in in C: "))

const = a
linear = b/T1
quad = c/(pow(T1,2))
cubic = d/(pow(T1,3))

R1 = Rt*exp(const + linear + quad + cubic)

const = a
linear = b/T2
quad = c/(pow(T2,2))
cubic = d/(pow(T2,3))

R2 = Rt*exp(const + linear + quad + cubic)

dV = 5*( (Rt/(Rt+R2)) - (Rt/(Rt+R1)) )

print "Resistance at ", (T1-CEL), "C is ", R1, "k"
print "Resistance at ", (T2-CEL), "C is ", R2, "k"
print "The voltage signal is ", dV, "V" 




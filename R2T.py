# Suitable for 3,599 < R < 32,770

from math import log, pow

a = 3.3540170e-03
b = 2.5617244e-04
c = 2.1400943e-06
d = -7.2405217e-08
Rt = 10000

R = float(raw_input("Please enter resistance"))
const = a
linear = b*(log(R/Rt))
quad = c*(log(pow((R/Rt),2)))
cubic = d*(log(pow((R/Rt),3)))

T = 1/(const + linear + quad + cubic)

#print const
#print linear
#print quad
#print cubic

print "The temprature is ", T, "k"
T = T - 273.15
print "The temprature is ", T, "C"

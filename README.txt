#=============================================
#~~~~~  Arduino Temprature Controller ~~~~~~~
#---------------------------------------------
A simple(ish) program for very high stability
temprature control for scientific purposes, 
using only hobbyist components

This folder should contain the following files and directories

Key Files
------------
	- User_Interface.py
	- Arduino_TempController
		- Arduino_TempController.ino
	- README.txt
	- Project Report

The file User_Interface.py is a python 2.7 program
that can run on a host computer and communicate 
with the Arduino, graphing tempratures and changing
set points on the fly. NOTE any changes to set points
will be lost when/if the arduino is reset.

Arduino_TempController.ino is the Arduino sketch (program)
that needs to be compiled and loaded onto the Arduino.
This program is standalone and once the Arduino is running
no further input from the PC is required. This is the file
to permanentley edit the setpoints for the temprature controller

README.txt is this file

Other Required Programs
------------------------
	- GUI.py
	- Data
	
GUI.py is a python module required for displaying the 
Graphical User Interface for the User_Interface program.
This requires PyQt which is released under the full GPLv3!

Data is the Data folder. 

Utilities
-------------
	- EpochToTime.py
	- plotData.py
	- ArduinoSim.py
	- R2T.py
	- T2R.py
	- cpustatus.sh

EpochToTime is a simple python utility to convert the
time used for data (seconds since the UNIX Epoch (1st Jan 1970)
into a human readable time

plotData.py This is a python utility to plot data output
by the User_Interface program. It accepts one argument on
the command line ie python plotData.py Data/FileName.dat

ArduinoSim.py This is a little python utility for
emulating the Arduino. It just generates random numbers
near 21 and outputs them over serial. You need to 
setup a serial loop using socat, this program is LINUX ONLY.
This program is based hevily on a Stack Exchange solution

R2T.py and T2R.py These progams generate a resistance 
from a temprature or vica a versa for a thorlabs 10k

cpustatus.sh This is a shell script to moniter the 
core temprature of a raspberry pi. This is not
my work and is based of a stack exchange solution










/*#=============================================
#~~~~~  Arduino Temprature Controller ~~~~~~~
#---------------------------------------------
# Interface Code
# V1.0
# Author -  Aaron Jones
# Date: 25/03/2015
# Copyright Aaron Jones, 2015
#---------------------------------------------
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------
To Do list
- Remove unused classes
- Remove unused code
- Look at how I am sending data, high enough accuracy?

#
# The following code is designed to be used 
# with an Arduino Nano and the custom circuit
# designed and built by Aaron Jones, 4th Yr MSci
# Physics Student, University of Birmingham.
#
# There are two modules. The first module is called
# the user module and runs on a computer 
# connected to the Arduino and allows the setting
# of variables and output of performace data.
# This is the second module called the control module
# and that runs on the Arduino actually controllingDominic 
# the temprature.
#
# Concept
# The concept is that the Arduino controls
# the temprature using a 16 Bit ADC using an I2C
# link. Proccess this infomation in this script,
# and then outputs this to a DAC back over the I2C
# link.  The voltage from the DAC is proccessed by a
# analouge circuit to control a peltier junction.
#
# A report was written on this program
# as part of a 4th Yr Project. This explains
# the method in more detail
# and contains circuit diagrams.
#
# Dependancies
# - User_Interface.py 
# - Control Module (This one)
# - Wire libary
#
# Acknowlagements
# The code used to access the timer2 function is from: 
# http://forum.arduino.cc/index.php?topic=62964.0-
# The author is davekw7x 
#
# The code used for the 24 Bit ADC is modified from Iain MacIvers
# work in 2014 for the University of Birmingham, Cold Atoms,
# GG-TOP group studies
#
# The PID has been modified from http://playground.arduino.cc/Code/PIDLibrary
# and is written by Brett Beauregard, contact: br3ttb@gmail.com
# Because it is under a stricter licence, it is stored sepertley
# The software can be used with an alternative algorithm under the LGPL
# or the PID under the fuller GPL

# Command Characters
# The following characters are used for sending commands
# between the two programs
# - m -> Message, the next line will be a message to the user
# - h -> Handshake, please acknowledge that the script is running
# - d -> Data
#
# Constants
# In this section there are a number of user tuneable constants
# These are globally accessible and called when required
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# */
/* ------ Common Constants ------- */
// After each reset, the following constants
// will be set by the Arduino. These can be
// overridden by a connected PC, but the change
// will not be permentant until set here

// ****NOTE**** Values MUST be specified for 6 AND ONLY 6 IO channels even if less are in use

// Set Temprature
// --------------
// The default set tempratures for the items, set any unused sensors to 21
// in degrees celius
// Typical Value = 25;
const double DEFAULT_SET_TEMPRATURE[6] = {19,23,25,25,25,25};

// Desired Accuracy
// --------------
// The accuracy with which the temprature must be stable to.
// If inside the range
// (SETPOINT - ACCURACY) < Temp < (SETPOINT + ACCURACY)
// the system will be regarded as stable and the stable
// led will light. Upon exiting this range the user will
// be informed
// Typical Value = 0.01;
const double ACCURACY[6] = {0.01,0.01,0.01,0.01,0.01,0.01};

// Proportional Constant
// --------------
// Typical Value = 4;
const double DEFAULT_PROPORTIONAL_CONSTANT[6] = {2,2,2,2,2,2};

// Intergral Constant
// --------------
// Due to the high sample frequency this typically has a low value
// Typical Value = 0.001;
const double DEFAULT_INTERGRAL_CONSTANT[6] = {5e-6,0,0,0,0,0};

// Differntial Constant
// --------------
// Due to the high sample frequency this typically has a low value
// Typical Value = 0.1;
const double DEFAULT_DIFFERNTIAL_CONSTANT[6] = {0,0,0,0,0,0};

// Type of Sensor
// --------------
// List of possible sensors (to add a sensor please add the relevent code to the function "volt2temp" )
enum SensorType {THORLABS_10K, AD590}; //Possible types of sensor

// Enter the sensors connected to ADC's 1-6 here
const int SENSORS[6] = {THORLABS_10K, THORLABS_10K, THORLABS_10K, 
                        THORLABS_10K, THORLABS_10K, THORLABS_10K};
                        
// Number of Channels in Use
// --------------
// Please enter the number of channels that are actually in use
// Channels must be used sequentially (i.e. you cant use channels 1,3 & 6, you must use 1, 2 & 3)
// This must be between one and 6
const int NUMBER_OF_IO_CHANNELS = 1;

// REVSERSE
// --------------
// If you get divergent behaviour, the peliter may be the wrong way up
// intead of trying to change that, change this from false to true
const boolean UP_SIDE_DOWN[6] = {false,false,false,
                                false,false,false};

/* ------- Tuning for High Accuracy Systems ------ */
// These are constants that need to be changed for 
// very high performace systems and all relate to the measurement
// of temperature

// Five Volts
// ---------
// The system is designed with power supply noise
// rejection in mind, but none the less for very
// high performace systems, enter the exact value of 
// 'five volts' that is supplied
// measure this between the less positive side of R5
// and ground for all 3 boards
const double FIVE_VOLTS[3] = {5.06, 5.0, 5.0};

// Referance Voltage
// ---------------
// The DAC provides an internal referance at 2.5V, this is very stable
// and the circuit is very sensitive to fluctuations in it,
// please measure this voltage (pin 10 on the DAC (AD5667R))
const double REFERANCE_2_5[3] = {2.49, 2.5, 2.5};

// Minus Voltage
// ---------------
// The 24 bit ADC is a differance ADC and so takes a 
// second voltage referance, this helps with 
// power supply rejection noise by forming a whetstone bridge style system
// Measure the voltage between R9 and R10
// This varies due to the exact resistances in the system
const double V_MIN[3] = {2.436, 2.5,2.5};

// Bottom Resistor
// ---------------
// The bottom resistor in the potential divider configution
// for each board in your system measure R11 and then R8
const double RESISTOR_BOTTOM[6] = {10.01e3, 10.01e3, 10e3, 10e3,10e3, 10e3};
//           = {Board 1-A, Board 1-B, Board 2-A, Board 2-B, Board 3-A, Board 3-B}

/* ------- Less Common Constants ----- */
// These are constants which are tuneable, but typically
// do not vary in every implemtation of the system
// They are still tuneable

// Output data to terminal?
// The program outputs data in data format but can also be
// configured to output routine messages to be displayed in terminal
// set this to true to enable this
// default = false
const boolean outputToTerminal = true;

// The value of Celsius in kelvin 
// -----------------------------
// The PID actually works in kelvin and converts to
// celsius on reciept and transmission of data
const double CELSIUS = 273.15;

// Maximum Temperature (kelvin)
// ------------------
// This is the maximum temperature that the system is allowed to go to
// If this temperature is reached the peltier and PID is disabled
const double MAX_ALLOWED_TEMP = 45 + CELSIUS;

// Minimum Temperature (kelvin)
// ------------------
// This is the minimum allowed temperature that the system is allowed to go to
// If it is exceeded then the peltier and PID are disabled
const double MIN_ALLOWED_TEMP = 15 + CELSIUS;

// Average Time
// --------------
// The number of samples over which to average the data
// Typical value is 10 Seems to fail above 25 dont know why
// Each sample takes around 5ms to collect therefor typical average time is 5*20 = 100ms
const int AVERAGE_LENGHT = 20;

// Report Frequency
// --------------
// Please specify a time in milliseconds between
// the the Arduino sending data to the PC
// If REPORT_FREQUENCY_TIME = 0 The Arduino will output
// after every measurement
// Typical Value = 500;
const int REPORT_FREQUENCY_TIME = 500;

// Stable Count
// --------------
// The number of successive 'good' measurements
// before we can consider the system stable. Should be
// at least twice the value of the (average time/5)
// 1 second is approx 200
// Typical Value = 100;
const int STABLE_COUNT = 100;

// Flash Frequency
// --------------
// Pin 13 flashes after every X interations of
// the stabilisation loop. Ie if X = 1
// then every time a new tempratue value is
// read the LED will change state (e.g. on -> off)
// This helps to detect if the program has crashed
// Typical Value = 1; (with ossiloscope)
// Typical Value = 100 (without scope)
const int FLASH_FREQUENCY = 200;
// You also need to set a flash pin
// If you have 6 sensors then there will be no free pins
// If you have less then you can configure a free pin
// Set to -1 to disable
const int FLASH_PIN = -1;
// NOTE: FLASH PIN MUST BE BETWEEN D0 and D7
// Check the pin advice in the documentation
// (This is because it uses the PORTD register, search registers arduino in google and edit if you wish)

/*
# ================================================
*/
// #### Header #####
// ==== Dependancies =====
#include <Wire.h>

// ==== Class Declarations =====
// ---- PID Class ----
// NB PID Class is GPL and must be removed for LGPL to be valid
class PID
{
  public:

  //Constants used in some of the functions below
  #define AUTOMATIC	1
  #define MANUAL	0
  #define DIRECT  0
  #define REVERSE  1

  //commonly used functions **************************************************************************
    PID(double* Input = NULL, double* Output = NULL, double* Setpoint = NULL,           // * constructor.  links the PID to the Input, Output, and 
        double Kp=0, double Ki=0, double Kd=0, int ControllerDirection=0);  //   Setpoint.  Initial tuning parameters are also set here
    
    void passNewPointers(double*, double*, double*); //* Function to set the pointers if the default constructor is used           
	
    void SetMode(int Mode);               // * sets PID to either Manual (0) or Auto (non-0)

    bool Compute();                       // * performs the PID calculation.  it should be
                                          //   called every time loop() cycles. ON/OFF and
                                          //   calculation frequency can be set using SetMode
                                          //   SetSampleTime respectively

    void SetOutputLimits(double, double); //clamps the output to a specific range. 0-255 by default, but
					  //it's likely the user will want to change this depending on
					  //the application
	
  //available but not commonly used functions ********************************************************
    void SetTunings(double, double,       // * While most users will set the tunings once in the 
                    double);         	  //   constructor, this function gives the user the option
                                          //   of changing tunings during runtime for Adaptive control
    void SetControllerDirection(int);	  // * Sets the Direction, or "Action" of the controller. DIRECT
					  //   means the output will increase when error is positive. REVERSE
					  //   means the opposite.  it's very unlikely that this will be needed
					  //   once it is set in the constructor.

    void clearHistory();                  // Added by AJ on 15/12/2014, clears history by setting ITerm = 0;
							  
  //Display functions ****************************************************************
	double GetKp();						  // These functions query the pid for interal values.
	double GetKi();						  //  they were created mainly for the pid front-end,
	double GetKd();						  // where it's important to know what is actually 
	int GetMode();						  //  inside the PID.
	int GetDirection();					  //

  private:
	void Initialize();
	
	double dispKp;				// * we'll hold on to the tuning parameters in user-entered 
	double dispKi;				//   format for display purposes
	double dispKd;				//
    
	double kp;                  // * (P)roportional Tuning Parameter
        double ki;                  // * (I)ntegral Tuning Parameter
        double kd;                  // * (D)erivative Tuning Parameter

	int controllerDirection;

        double *myInput;              // * Pointers to the Input, Output, and Setpoint variables
        double *myOutput;             //   This creates a hard link between the variables and the 
        double *mySetpoint;           //   PID, freeing the user from having to constantly tell us
                                      //   what these values are.  with pointers we'll just know.
			  
        double ITerm, lastInput;

        double outMin, outMax;
        bool inAuto;
};

// ---- USB Class ----
class io
{
  private:
    int mode;  // mode = 0: no pc connected
               // mode = 1: PC connected, user script not running
               // mode = 2: PC connected, user script running, output debug messages only
               // mode = 3: supress all output
               // mode = 99: connection yet to be determined
    void clearin();
    void Connect();
        
  public:
    // Configuration Functions
    io();
    char checkPC();
    
    // Sending Data
    void sendCommand(char command);
    void printMessage(String message);
    void sendData(double data[]);
    
    // Sending Complex Messages
    boolean openPartMessage(); // Returns false if PC not present
    boolean openPartMessage(int activeMode); // Send message only if mode activeMode
    void closePartMessage(); // Disable this functionallity
    void updateUser(int ch, double tempC, double Vout_Signed);
    
    // Recieving data
    char readchar();
    double readfloat();
    int readint();
};

// ---- DAC Class ----
class DAC
{ 
  private:
    const int address[3] = {0x0E,0x0F,0x0C}; // Address corresponding to N/C, GND and 5V on addr pin respectivly
    void transmissionFail(int failCode, int addri);
    int NumDAC;
  public:
    DAC(int NumDACsIn);
    int output(double value, int channel);
};

class ADC24 // Class for 24 bit ADC
{ 
  private:
    byte PSCLK; // Bytes to hold the pins for the clock and data
    byte PDATA[6];  //Array to hold ADC pin numbers
    int NumADCs;
    
    void AwaitPulse();  //Functions to control timings 
    void AwaitAdc();    // for the ADC
    void ResetAdc();
    
  public:
    ADC24(int NumADCsIn);  // Pin config is set, SLK on D8, DOUT/DRDY on pins 2-7, just need to know which are active
    void Update();
    float lastResult[6];
};

// ==== Function Declarations =====
double volt2Temp(double voltage, double fiveV, int arraySize, double temps[], int thisSensor);
void D11clock();
void constantError(String message);
void(* resetFunc) (void) = 0; // declare reset function at address 0

void setup()
{
    Serial.begin(115200); // Setup serial communiction
    Serial.println("mArduino staring up..."); //Inform user
    Serial.println("Checking Constants");
    {
        if (AVERAGE_LENGHT < 1)
           {constantError("The averaging lenght must be greater than 1");}
    }
    D11clock(); // Setup a hardware clock output on pin 11
    Serial.println("m Entering main function");   // Inform user
}

// #### Function Defonitions ####
// ==== Main Program =====
void loop()
{
  // ####  Setup ####
  // ==== Call Functions ====
  Wire.begin(); //Join I2C bus as master
  
  // ==== Set up classes ====
  // ---- IO ----
  io ser;    // Open class for IO communication via serial
  ser.printMessage("Setting up variables...");
  
  // ==== Set Pins ===
  const int HCpin[6] = {12,13,14,15,16,17};    // The pin that controlls hot / cold
  
  if (FLASH_PIN != -1)
  {
     pinMode(FLASH_PIN, OUTPUT);
     digitalWrite(FLASH_PIN,HIGH); 
  }
  int runningLoop =0;
  
  // ---- DAC ----
  //DEBUG
  DAC dac(NUMBER_OF_IO_CHANNELS);    //Set up a class for the DAC
  
  // ---- ADC's ----
  ADC24 myLaser(NUMBER_OF_IO_CHANNELS);      // Delcare the ADC24 out here so it is visible
  
  // ---- PID ----
  //Define the PID variables
  double Setpoint[6], Input[6], Output[6];
  double accuracy[6];
  boolean stable = false;

  //Specify the links and initial tuning parameters (split over two lines)
  PID myPID[6];
  
  for (int i = 0; i < NUMBER_OF_IO_CHANNELS; i++)
  {
    
    // Setup the PID controllers
    Setpoint[i] = CELSIUS + DEFAULT_SET_TEMPRATURE[i];    // Set to 20 deg C 
    myPID[i].passNewPointers(&(Input[i]), &(Output[i]), &(Setpoint[i]));  //Load pointers for input and output
    
    myPID[i].SetTunings(DEFAULT_PROPORTIONAL_CONSTANT[i], //Load tunings
                        DEFAULT_INTERGRAL_CONSTANT[i], 
                        DEFAULT_DIFFERNTIAL_CONSTANT[i]);
    
    myPID[i].SetOutputLimits(-1, 1);  // Set limits to -3.5 -> 3.5V, can send this straight to DAC
    
    Input[i] = Setpoint[i]; // Initialise input with the setpoint so we can execute a test compute
    
    //Setup the Output Pins
    pinMode(HCpin[i], OUTPUT);  // Set pin to be an output
    digitalWrite(HCpin[i], HIGH);  // Set pin to high, it needs a value, I pick high
    
  }
  
  for (int i = 0; i < NUMBER_OF_IO_CHANNELS; i++)
  {
    //turn the PIDs on
    myPID[i].SetMode(AUTOMATIC);
    myPID[i].clearHistory();  // Clear the PID history to ensure no weirdness
  }
  
  ser.printMessage("Begin temprature control loop...");
  while(true) // Loop forever
  {
    // Check that the PC is still there and if it want to change anything
    char pcVal = ser.checkPC();
    switch (pcVal)
    {
      case 'v':
      {            // Case v, the PC whishes to know the values of Kp, Ki, Kd and the Setpoint
        int ID = ser.readint();
        if(ser.openPartMessage(3)) {
          Serial.print("V=");
          Serial.print(myPID[ID].GetKp(),8);
          Serial.print(','); //  csv delimited  
          Serial.print(myPID[ID].GetKi(),8);  //repete
          Serial.print(','); //  csv delimited  
          Serial.print(myPID[ID].GetKd(),8);
          Serial.print(','); //  csv delimited  
          Serial.print((Setpoint[ID])-CELSIUS,4);
          Serial.print(',');
          Serial.print(accuracy[0]);
          ser.closePartMessage();
        }
      }
        break;
        
      case 'V':                      //PC wishes to set new values of Kp, Ki, and Kd
      {
        int ID = ser.readint();
        float Kp = ser.readfloat();    //Read the new values
        float Ki = ser.readfloat();
        float Kd = ser.readfloat();
        float SetIn = ser.readfloat();
        float accuracyIn = ser.readfloat();
        char endline = ser.readchar();  //Check character
        
        if (endline == 'V')  // If check isnt okay, ignore
        {
          Setpoint[ID] = SetIn + CELSIUS;  //otherwise set the new values, SetPiont is passed by pointer, so just set the new value
          myPID[ID].SetTunings(Kp, Ki, Kd);  //Use function to set new tunings
          accuracy[ID] = accuracyIn;
        }
      }
        break;
        
      case 'n':
        break;
        
      case 'R':
        resetFunc();
        break;
    }
    
    // Read ADC's
    myLaser.Update();
    double voltage[2];
    
    // Cycle Through Calculating changes required and outputting
    for (int i = 0; i < NUMBER_OF_IO_CHANNELS; i++)
    {
      // Convert to temprature and average
      voltage[i] = V_MIN[i/2] + REFERANCE_2_5[i/2]*(myLaser.lastResult[i]);  // If voltage > FIVE_VOLTS then intergral winding may occur (integer devision rounds down)
            
      static double allTemps[6][AVERAGE_LENGHT];  //array to hold temprature value to be averged over
      
      // Convert the voltage to a temperature and perform a moving average
      double temp = volt2Temp(voltage[i],FIVE_VOLTS[i/2],AVERAGE_LENGHT,allTemps[i],i); // Pass the 5V value to this function, if using constant current just pass a zero, its not required
      
      // If the input is outside the allowed ranges then we don't want to compute new PID
      // parameters as this will cause intergral winding, lets just disable the 
      // Peltier and let everything thermalise to room temerature
      if (temp > 0)  // if the last measured temprature was outside range then we will get a return value less than 0, just ignore
      { 
          Input[i] = temp;

        // Compute the new PID parameters
        myPID[i].Compute();
        
        if (UP_SIDE_DOWN[i] == false)
        {
          if (Output[i] > 0) { digitalWrite(HCpin[i], HIGH); dac.output(Output[i],i);}
          else {digitalWrite(HCpin[i], LOW); dac.output(0.0-Output[i],i);};
        }
        else
        {
          if (Output[i] > 0) { digitalWrite(HCpin[i], LOW); dac.output(Output[i],i);}
          else {digitalWrite(HCpin[i], HIGH); dac.output(0.0-Output[i],i);};
        }
      }
    }

    //Flash LED to show that the program is looping the loop counter is to allow some gearing
    if (FLASH_PIN != -1)
    {
      runningLoop++;
      if(runningLoop == FLASH_FREQUENCY)
      {
        PORTD ^= (1 << FLASH_PIN);
        // This is using port level IO
        // To toggle a pin otherwise uses two lines of code
        // and looks a bit ugly.
        // The command is ^ is an exclusive OR
        // (1 << X) means bit shift 
        // 0000 0001 left by X places. The effect is a toggle
        // Search toggle a bit c++
        // PORTD is the register that outputs to the pins
        // This is 80x faster than digitalWrite
        runningLoop = 0;
      }
    }
    
    // Print data - Only print every second
    // The rpi is pretty slow and even then serial isnt so quick
    static unsigned long int timelast = 0;
    unsigned long int timenow = millis();
    
    if (timenow > timelast + REPORT_FREQUENCY_TIME)
    { 
      ser.sendData(Input);
      if (outputToTerminal == true)
      {
        for (int i = 0; i < NUMBER_OF_IO_CHANNELS; i++)
          {ser.updateUser(i,(Input[i]-CELSIUS), Output[i]);}
      }
      timelast = timenow;
    }
  }
  // End of loop
}

// ==== Voltage to Temprature =====
double volt2Temp(double voltage, double fiveV, int arraySize, double temps[], int thisSensor)
{
  double tNow;
  
  if (SENSORS[thisSensor] == THORLABS_10K)
  {
    // First Calculate a resitance from a voltage
    // === Using a Simple Potentiometer ====
    double resistance = RESISTOR_BOTTOM[thisSensor]*((fiveV/voltage) - 1.0);  // This is just the potential divider formula re-arranged
    
    // Next calculate a temprature (in Kelvin!) from a voltage
    const double R25 = 10000; // Resistance at 25 Deg C (298.15 Deg K)
    const double constTerm = 3.3540170e-3;
    double linearTerm = (2.5617244e-4)*log(pow(resistance/R25,1));
    double quadraticTerm = (2.1400943e-6)*log(pow((resistance/R25),2));
    double cubicTerm = (-7.2405219e-8)*log(pow((resistance/R25),3));
    
    tNow = 1/(constTerm+linearTerm+quadraticTerm+cubicTerm);
  }
  else if (SENSORS[thisSensor] == AD590)
  {
     // V= IR
     double current = voltage/RESISTOR_BOTTOM[thisSensor];
     
     // 1uA per k
     tNow = current / 1e-6;
    
  }
  else { constantError("No Code provided for enumerated sensor! Please add the sensor to the enumeration options and the relevent code in volt2temp"); }
  
  // Check to see if this is a valid temp
  if ((tNow > MAX_ALLOWED_TEMP) or (tNow < MIN_ALLOWED_TEMP))
  {
     return -1; 
  }
  
 //Now do a N-Point moving average to eliminate some noise
  static int arrayIndex = 0;  //Point to the element in the array to be updated
  static bool firstCall = true;  //Just for array initislisation
  //If this is the first time then let all the variables have this temprature
  //and initilise the array
  if (firstCall)
  {
     int i = 0;
     while ( i < arraySize)
     {
       temps[i] = tNow;
       i++;
     }
     firstCall = false;
  } 
  else {
    if(arrayIndex == arraySize) arrayIndex = 0;
    
    temps[arrayIndex]=tNow;
    arrayIndex++;
  }
  double tempSum = 0;
  for (int i = 0; i<arraySize; i++) { tempSum = tempSum + temps[i];}
  
  return (1.0/arraySize)*tempSum;
}

// This function is modified from davekw7x, see the acknowlegemets section for details
// Use of timer2 to generate a signal for a particular frequency on pin 11
void D11clock()
{
    // Give the pin connected to the 0C2A comparitor register a name
    const int freqOutputPin = 11;   // OC2A output pin for ATmega328 boards
    //const int freqOutputPin = 10; // OC2A output for Mega boards
    
    // Constants are computed at compile time
    
    // If you change the prescale value, it affects CS22, CS21, and CS20
    // For a given prescale value, the eight-bit number that you
    // load into OCR2A determines the frequency according to the
    // following formulas:
    //
    // With no prescaling, an ocr2val of 3 causes the output pin to
    // toggle the value every four CPU clock cycles. That is, the
    // period is equal to eight slock cycles.
    //
    // With F_CPU = 16 MHz, the result is 2 MHz.
    //
    // Note that the prescale value is just for printing; changing it here
    // does not change the clock division ratio for the timer!  To change
    // the timer prescale division, use different bits for CS22:0 below
    const int prescale  = 1;
    const int ocr2aval  = 3; 
    // The following are scaled for convenient printing
    //
    
    // Period in microseconds
    const float period    = 2.0 * prescale * (ocr2aval+1) / (F_CPU/1.0e6);
    
    // Frequency in Hz
    const float freq      = 1.0e6 / period;

    pinMode(freqOutputPin, OUTPUT);
    
    // Set Timer 2 CTC mode with no prescaling.  OC2A toggles on compare match
    //
    // WGM22:0 = 010: CTC Mode, toggle OC 
    // WGM2 bits 1 and 0 are in TCCR2A,
    // WGM2 bit 2 is in TCCR2B
    // COM2A0 sets OC2A (arduino pin 11 on Uno or Duemilanove) to toggle on compare match
    //
    TCCR2A = ((1 << WGM21) | (1 << COM2A0));

    // Set Timer 2  No prescaling  (i.e. prescale division = 1)
    //
    // CS22:0 = 001: Use CPU clock with no prescaling
    // CS2 bits 2:0 are all in TCCR2B
    TCCR2B = (1 << CS20);

    // Make sure Compare-match register A interrupt for timer2 is disabled
    TIMSK2 = 0;
    // This value determines the output frequency
    OCR2A = ocr2aval;
    
    Serial.println("m Outputting on pin D11 with");
    Serial.print("mPeriod    = ");
    Serial.print(period); 
    Serial.println(" microseconds");
    Serial.print("mFrequency = ");
    Serial.print(freq); 
    Serial.println(" Hz");
}


void constantError(String message)
{
   while(true)
   {
      Serial.println('m' + message);
      Serial.println(
        "m The Arduino will not run untill recompiled with the correct constants");
      delay(5000);
   } 
}


// ##### Define Classes ######
// ==== Define IO Class =====
io::io()
{
  pinMode(13, OUTPUT);
  clearin();		// clear anything waiting on the buffer
  mode = 0;	
  Connect();
}
void io::Connect()
{
  sendCommand('h');  // Send hello/handshake command
  delay(50);
  // dont use the readchar function it might not return
  char rv = Serial.read();
  if (rv == 'h') {mode =2;}
  else { mode = 1;}
}
// --- Write functions ---
void io::sendCommand(char command) {Serial.flush(); Serial.println(command);} 
//print command and new line
void io::printMessage(String message) {if(mode==2) {Serial.flush(); Serial.print('m'); Serial.println(message); }} 
// print message character followed by the message and a new line
void io::sendData(double data[]) {
//Sending floats at accuracies < 1e-8 causes problems so we will round off here to avoid any problems
  if(mode==2){
    int i = 0;  // Create Counter
    Serial.flush();    // Flush and initiate data transfer message
    Serial.print('d');
    
    while (i < NUMBER_OF_IO_CHANNELS) {
      Serial.print(data[i]-CELSIUS,8);
      Serial.print(',');
      i++;
    }
    
    while (i < 6){
      Serial.print("OFF");
      Serial.print(',');
      i++;
    }
    Serial.println();
  }
}

void io::updateUser(int ch, double tempC, double Vout_Signed) {
  if(mode==2) {
    Serial.flush();  //Wait for any output to finish
    Serial.print("m Ch: ");  //Signal message for screen
    Serial.print(ch);
    Serial.print(" T=");  //Write first few characters
    Serial.print(tempC,3);  //Output temprature to 3dp
    Serial.print("C DA: ");  
    Serial.print(Vout_Signed,3);  //Output voltage out
    Serial.print(" Ia:");        
    double I = (Vout_Signed/2.11)/0.9;
    // The current is the (voltage/amplification_of_signal)/Resitance(0.7 Ohms + 0.2 track on PCB)
    Serial.print(I,3);  //Output current to 3dp
    Serial.println();
  }
}
boolean io::openPartMessage() {if (mode==2) {Serial.print('m'); return true;} else {return false;}}
boolean io::openPartMessage(int activeMode) {if (mode = activeMode) {Serial.print('m'); return true;} else {return false;}}
// start new line with message character
//void io::printPartMessage(String message) {Serial.print(message);}
//print message
void io::closePartMessage() {Serial.print('\n');}
//print new line

// --- Read functions ---
double io::readfloat() {float f; if(!Serial.available()) {delay(10);} Serial.readBytes((char*)&f, sizeof(f)); return f;}
// declare float, wait until data becomes available then read it from USB
int io::readint() { if(!Serial.available()) {delay(10);} return Serial.read();}
// wait for data to become available the return and cast to integer
char io::readchar() { if(!Serial.available()) {delay(10);} return Serial.read();}
// wait for data to become available the return and cast to integer
void io::clearin() { while(Serial.available()) {Serial.read();}}
char io::checkPC()
{
   static int timeLast = 0;
   static int timeNow = 0;
   timeNow = millis();
  
   char pcCommand = 'n';
   if(Serial.available())
   {
     pcCommand = readchar();
     
     if (pcCommand == 'h') {mode = 2; timeLast=timeNow; pcCommand = 'n';}  // if a handshake has been sent by the pc, we can accept it and continue
     else if (pcCommand == 'S') {
       mode = 3;
       timeLast = timeNow-4000;  // If the pc doesnt set us back to normal mode by handshaking within 1 second we will assume connection wait for one second and then attempt to recconect to it
       pcCommand = 'n';
       sendCommand('S');
     }  //If 'S' has been sent, set mode to 3
   }
   else if ((timeNow - timeLast)>5000)  //Run every 5 seconds, unless data has just been recieved
   {
     Connect();
     timeLast = timeNow;     
   }
   else if (timeNow < timeLast) {timeLast = 0;}  // After 50 days millis overflows, this handels that
   
   // Finnaly write the state of the system to an LED
   if(mode == 2){ digitalWrite(13,HIGH);}
   else { digitalWrite(13,LOW);}
  
  return pcCommand;
}

// ==== Define DAC Class =====
DAC::DAC(int NumDACsIn)
{
  NumDAC = (NumDACsIn+1)/2;  //Use integer division, the result is always rounded down, we
                             // want rounded up so add 1.
                             // Each DAC has 2 outputs so the actual number is the apparent number + 1
                             
  int tranmission_status = 0;
 
  // Reset each of the three ADC's in turn
  for(int i = 0; i < NumDAC; i++)
  { 
    //Reset the DAC registers
    Wire.beginTransmission((uint8_t)address[i]);
      Wire.write((uint8_t) 0b00101000);  //write the command byte as RESET (C2=1, C1 = 0, C0 = 1)
      Wire.write((uint8_t) 0x00);        // Most Sig Data Byte (irrelevent)
      Wire.write((uint8_t) 0x01);        // Least Sig data byte, LSB set to 1 for full reset
    tranmission_status = Wire.endTransmission();
    if (tranmission_status != 0) {transmissionFail(tranmission_status,address[i]);}
    
    //Disable LDAC -- The LDAC pin is grounded so it shouldnt matter anyway
    Wire.beginTransmission((uint8_t)address[i]); 
      Wire.write((uint8_t) 0x30);      // Open LDAC register setup
      Wire.write((uint8_t) 0x00);      // MSB irrelevent
      Wire.write((uint8_t) 0x03);      // set last two bits high to disable the LDAC pin
    tranmission_status = Wire.endTransmission();
    
    //Enable internal referance
    Wire.beginTransmission((uint8_t)address[i]);
      Wire.write((uint8_t) 0x38);
      Wire.write((uint8_t) 0x00);
      Wire.write((uint8_t) 0x01);
    tranmission_status = Wire.endTransmission();
  }
}

void DAC::transmissionFail(int failCode, int addri)
{
  String failCode_s = String(failCode);
  String addr_hex = String(addri,HEX);
  String addr = String(addri);
  
  constantError("Tranmission to DAC on ADDR HEX: " + addr_hex + " NUM: " + addr +
                " failed with code : " + failCode_s);  
  
}

int DAC::output(double value, int channel){
  
 uint8_t command; 
 if(channel & 1) {command = (uint8_t)0b00000001;}  // If the last bit of the channel is 1,
 else            {command = (uint8_t)0b00000000;}  // the channel is odd (1,3,5)
                                                  // therefore output on B
                 
  const unsigned int DAC_bits = 0b1111111111111111;
  float ratio = value/(2*2.37);
  if(ratio >= 1) ratio = 1;
  
  unsigned int binary_out = ratio*DAC_bits;  //Convert to binary, since using an internal referance do this conversion here
  //binary_out = 0b0000000000000001;
  
  int tranmission_status;
  Wire.beginTransmission((uint8_t) address[channel/2]); // The address is found by dividing by two
                                                         // and rounding down, then looking up in array
    Wire.write(command);  //write to config register
    Wire.write((uint8_t)(binary_out>>8));  //write first 8 bits (MSB)
    Wire.write((uint8_t)(binary_out & 0xFF));  //write to second 8 bits (LSB)
  tranmission_status = Wire.endTransmission();
  return tranmission_status;
  
}


// ==== Define ADC24 Class =====
ADC24::ADC24(int NumADCsIn)
{
  // Bytes to hold the pins for the clock
    PSCLK = 0; // Pin 1, register B therefore D8.
  
  // Load number of ADC's
   NumADCs = NumADCsIn;
  
  // Setup pins for output
    pinMode(PSCLK+8, OUTPUT);
    
  for(int i=0; i<NumADCs; i++) {  
    PDATA[i] = i + 2;  // The pins are 2,3,4,5,6,7 for ADCS 0,1,2,3,4,5
    pinMode(PDATA[i], INPUT);  // Set output
  }
  
  ResetAdc();
  
  // Finnaly initalize array so other functions can access the converted data
  for(int i = 0 ; i<6 ; i++) {
    lastResult[i] = 0;
  }
  
}

// With SCLCK left low, the ADC will only pulse when data is ready
// in this case it will low, then resume high
void ADC24::AwaitPulse() {
  while(!(PIND&(1<<(PDATA[0]))));
  while(PIND&(1<<(PDATA[0])));
}

// Wait the ADC to complete at least one cycle
void ADC24::AwaitAdc() {
  AwaitPulse();
  AwaitPulse();
}

// To reset the ADC pull the SCLK high for 5 data periods 
void ADC24::ResetAdc()
{
  PORTB |= (1 << PSCLK);
  delayMicroseconds(1440);
  PORTB &= 0b11111111^(1<<PSCLK);
}

void ADC24::Update()
{  
  // Setup variables 
  long int inputVar[6] = {0,0,0,0,0,0};  //result variable
  long int mask = 1L<<23;  //mask to add data (write int 1, force to be long, then bitshift by 23)
  
  byte HighMask = 1 << PSCLK;
  byte LowMask = 0b11111111^HighMask;
  
  AwaitAdc(); // Wait for ADC to become ready
  delayMicroseconds(2);
  
  // Loop through and recieve each bit
  for(int i = 0; i < 24; i++)
  {
    PORTB |= HighMask;  //Toggle SCLK high to send new bit
    delayMicroseconds(2);  // Allow line to settle
    for(int j = 0; j< NumADCs; j++){
      inputVar[j] |= (PIND&(1<<(PDATA[j])))?mask:0;  // OR assignment, (if new bit is high, OR with mask, else 0)
    }
    delayMicroseconds(2);  // Allow line to settle
    PORTB &= LowMask;  //Return SCLK low
    mask = mask>>1;  // Decrement 
  }
  
  // Declare number to convert 0 - 2^23 integer -> 0-1 decimal
  const float divisor = 8388607;  // (2^23 - 1) but you cannot store it like that
  
  for(int i=0; i<NumADCs; i++) {  
     // Identify if negative (two's compliment)
    if((1L<<23) & inputVar[i]) //Mask to identify if the number is negative
    {
       inputVar[i] = ~inputVar[i];  // Invert all the bits
       inputVar[i]++;               // Add one
       inputVar[i] &= 0x007fffff;   // Set bits 23 - 31
       // This is required because we are dealing with a 24 bit number stored in a 32 bit integer
       
       // We now have a positive number and can return it as normal as long as we make it negative
       lastResult[i] = -((float)(inputVar[i]))/divisor;
    }
    else {lastResult[i] = ((float)(inputVar[i]))/divisor;}   
    
  }
  return;
}

// ==== Define PID Class =====
// NB PID class is GPL and must be removed for LGPL to be valid
PID::PID(double* Input, double* Output, double* Setpoint,
        double Kp, double Ki, double Kd, int ControllerDirection)
{
	
    myOutput = Output;
    myInput = Input;
    mySetpoint = Setpoint;
    inAuto = false;
	
    PID::SetOutputLimits(0, 255);				//default output limit corresponds to 
								//the arduino pwm limits
    PID::SetControllerDirection(ControllerDirection);
    PID::SetTunings(Kp, Ki, Kd);
			
}
void PID::passNewPointers(double* Input, double* Output, double* Setpoint)
// Added by AJ, 07 / AUG / 2015
{
    myOutput = Output;
    myInput = Input;
    mySetpoint = Setpoint;
}

 
 
/* Compute() **********************************************************************
 *     This, as they say, is where the magic happens.  this function should be called
 *   every time "void loop()" executes.  the function will decide for itself whether a new
 *   pid Output needs to be computed.  returns true when the output is computed,
 *   false when nothing has been done.
 **********************************************************************************/ 
bool PID::Compute()
{
   if(!inAuto) return false;
   //Serial.println("Actually ran!");

    /*Compute all the working error variables*/
    double input = *myInput;
    double error = *mySetpoint - input;
    ITerm+= (ki * error);
    if(ITerm > outMax) ITerm= outMax;
    else if(ITerm < outMin) ITerm= outMin;
    double dInput = (input - lastInput);
     
    /* Serial.println(error);
     Serial.println(kp*error);
     Serial.println(ITerm);
     Serial.println(kd*dInput);*/
     
    /*Compute PID Output*/
    double output = kp * error + ITerm- kd * dInput;
     //Serial.println(output);
    if(output > outMax) output = outMax;
    else if(output < outMin) output = outMin;
	  *myOutput = output;
	  
    return true;
}


/* SetTunings(...)*************************************************************
 * This function allows the controller's dynamic performance to be adjusted. 
 * it's called automatically from the constructor, but tunings can also
 * be adjusted on the fly during normal operation
 ******************************************************************************/ 
void PID::SetTunings(double Kp, double Ki, double Kd)
{
   if (Kp<0 || Ki<0 || Kd<0) return;
 
   dispKp = Kp; dispKi = Ki; dispKd = Kd;
     
   kp = Kp;
   ki = Ki;
   kd = Kd;
 
  if(controllerDirection ==REVERSE)
   {
      kp = (0 - kp);
      ki = (0 - ki);
      kd = (0 - kd);
   }
}
 
/* SetOutputLimits(...)****************************************************
 *     This function will be used far more often than SetInputLimits.  while
 *  the input to the controller will generally be in the 0-1023 range (which is
 *  the default already,)  the output will be a little different.  maybe they'll
 *  be doing a time window and will need 0-8000 or something.  or maybe they'll
 *  want to clamp it from 0-125.  who knows.  at any rate, that can all be done
 *  here.
 **************************************************************************/
void PID::SetOutputLimits(double Min, double Max)
{
   if(Min >= Max) return;
   outMin = Min;
   outMax = Max;
 
   if(inAuto)
   {
	   if(*myOutput > outMax) *myOutput = outMax;
	   else if(*myOutput < outMin) *myOutput = outMin;
	 
	   if(ITerm > outMax) ITerm= outMax;
	   else if(ITerm < outMin) ITerm= outMin;
   }
}

/* SetMode(...)****************************************************************
 * Allows the controller Mode to be set to manual (0) or Automatic (non-zero)
 * when the transition from manual to auto occurs, the controller is
 * automatically initialized
 ******************************************************************************/ 
void PID::SetMode(int Mode)
{
    bool newAuto = (Mode == AUTOMATIC);
    if(newAuto == !inAuto)
    {  /*we just went from manual to auto*/
        PID::Initialize();
    }
    inAuto = newAuto;
}
 
/* Initialize()****************************************************************
 *	does all the things that need to happen to ensure a bumpless transfer
 *  from manual to automatic mode.
 ******************************************************************************/ 
void PID::Initialize()
{
   ITerm = *myOutput;
   lastInput = *myInput;
   if(ITerm > outMax) ITerm = outMax;
   else if(ITerm < outMin) ITerm = outMin;
}

/* SetControllerDirection(...)*************************************************
 * The PID will either be connected to a DIRECT acting process (+Output leads 
 * to +Input) or a REVERSE acting process(+Output leads to -Input.)  we need to
 * know which one, because otherwise we may increase the output when we should
 * be decreasing.  This is called from the constructor.
 ******************************************************************************/
void PID::SetControllerDirection(int Direction)
{
   if(inAuto && Direction !=controllerDirection)
   {
	  kp = (0 - kp);
      ki = (0 - ki);
      kd = (0 - kd);
   }   
   controllerDirection = Direction;
}

/* Status Funcions*************************************************************
 * Just because you set the Kp=-1 doesn't mean it actually happened.  these
 * functions query the internal state of the PID.  they're here for display 
 * purposes.  this are the functions the PID Front-end uses for example
 ******************************************************************************/
double PID::GetKp(){ return  dispKp; }
double PID::GetKi(){ return  dispKi;}
double PID::GetKd(){ return  dispKd;}
int PID::GetMode(){ return  inAuto ? AUTOMATIC : MANUAL;}
int PID::GetDirection(){ return controllerDirection;}
void PID::clearHistory() {ITerm = 0;}

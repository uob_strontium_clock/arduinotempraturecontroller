# ==========================
#    Arduino Simulator for Linux
#==========================
# Copyright Aaron Jones, 2015
# The following code sends data over a serial emulator as described in the following answer
# http://stackoverflow.com/questions/52187/virtual-serial-port-for-linux
#Complementing the @slonik's answer.
#You can test socat to create Virtual Serial Port doing the following procedure (tested on Ubuntu 12.04):
#Open a terminal (let's call it Terminal 0) and execute it:
#	socat -d -d pty,raw,echo=0 pty,raw,echo=0
#The code above returns:
#	2013/11/01 13:47:27 socat[2506] N PTY is /dev/pts/2
#	2013/11/01 13:47:27 socat[2506] N PTY is /dev/pts/3
#	2013/11/01 13:47:27 socat[2506] N starting data transfer loop with FDs [3,3] and [5,5]
#Open another terminal and write (Terminal 1):
#	cat < /dev/pts/2
#Open another terminal and write (Terminal 2):
#	echo "Test" > /dev/pts/3
#Now back to Terminal 1 and you'll see the string "Test".

import serial
import struct
import time
import numpy as np
ser = serial.Serial('/dev/pts/4', 9600)
ser.write('h\n')
out = [1,2,3,4,5,6]
while True:
	i = 0
	ser.write('d')
	while i<6:
		out[i] = 21 + np.random.random()
		ser.write(str(out[i]))
		ser.write(',')
		i = i + 1

	print out
	ser.write('\n')
	time.sleep(1)

#Comment
